const Twitch = require('twitch-js')
require('colors')

/*

  !VOTAR+NUMERODEVOTACION --> Elegir en que votacion estar
  !EMPEZAR --> HABILITAR VOTACION
  !TERMINAR --> TERMINAR VOTACION
  !RESET --> Vuelve a 0 votacion actual

  TODO:
  * En el json chiquito guardar un campo path que define el path a una imagen. El ingrediente con mayor cantidad de votos tendra otra distinta
  * Ver si node corre con el image coso o en otra compu.
  * Si corre en otra compu va a ver que ver si se hace un client/server tcp o si se hace una carpeta de red.
  EN ese caso, la compu que corre la visualización tendra un programa

 */

/** ***************************** INIT ************************************/

var state = require('./data/configuracion.json')
var votaciones = require('./lib/init.js')(state)
var admin = require('./lib/admin.js')(state, votaciones)
var save = require('./lib/saver.js')(state, votaciones)

setInterval(save, state.INTERVALO_GUARDAR)

/** *************************** TWITCH *************************************/
const options = {
  options: {
    debug: state.DEBUG
  },
  connection: {
    cluster: 'aws',
    reconnect: true
  },
  identity: {
    username: state.ID.USR,
    password: state.ID.PASS
  },
  channels: [state.CHANNEL]
}

const client = new Twitch.client(options)
client.connect()
// client.color('SpringGreen')

client.on('connected', onConnection)
client.on('chat', onChat)

console.log('estado actual de la votación'.yellow)
console.log(votaciones[state.atVotacion])

/** ************************** HANDLERS **********************************/

function onConnection (addr, port) {
  console.log('CONECTADO BOT AL CHAT'.green)
  client.action(state.CHANNEL, 'HOLA')
}

function onChat (channel, user, message, self) {
  // Mensajes de administrador
  if (isAdmin(user['display-name']) !== undefined) {
    console.log((user['display-name'] + ' >> ').red + message.yellow)
    admin(message)
  }
  // Mensajes de usuarios
  console.log((user['display-name'] + ' >> ').blue + message.yellow)

  if (state.votando !== undefined) {
    if (message.indexOf('!') === -1) {
      console.log('mensaje fuera de formato'.red)
    } else {
      var ing = message.split('!')[1].toLowerCase()
      if (votaciones[state.atVotacion][ing] !== undefined) {
        if (!votaciones[state.atVotacion][ing]['bloqueado']) {
          console.log(('nuevo voto' + ': ').green + ing.cyan)
          votaciones[state.atVotacion][ing]['cantidad'] += 1
        }
      } else {
        console.log('ingrediente inexistente'.red)
      }
    }
  } else {
    // ACA SE LE PUEDE MANDAR AL USUARIO QUE ESTA CERRADA LA VOTACION
  }
}

//* ************************* ADMIN ***************************************//

function isAdmin (usr) {
  return usr === state.ADMIN
}
