const fs = require('fs')
const path = require('path')
require('colors')

module.exports = save

function save (state, votaciones) {
  return function () {
    saveActualJSON()
    saveMainJSON()
    console.log('guardando...'.green)
  }

  function saveMainJSON () {
    fs.writeFile(path.join(process.cwd() + '/data/votaciones.json'), JSON.stringify(votaciones, null, 4), function (err) {
      if (err) console.error('error guardando archivo :(')
    })
  }

  function saveActualJSON () {
    var obj = {}
    var objPuntajes = {}
    var votActual = votaciones[state.atVotacion]

    calcularPorcentajes()
    // Sub obj porcentajes para guardar
    Object.keys(votActual).forEach(function (ingrediente, index) {
      obj[ingrediente] = votActual[ingrediente]['porcentaje']
    })

    // Sub obj puntaje para saber quien gana
    Object.keys(votActual).forEach(function (ingrediente, index) {
      objPuntajes[ingrediente] = votActual[ingrediente]['cantidad']
    })

    obj['imgGanador'] = setGanadorImg(objPuntajes)

    // Porque ese programa de pendorcha no sabe parciar un json q no es un array, aparentemente...
    var arrayDeMierda = []
    arrayDeMierda.push(obj)
    // arrayDeMierda.push(JSON.stringify({}, null, 4))

    // Guardar
    fs.writeFile(path.join(process.cwd() + '/data/votacionActual.json'), JSON.stringify(arrayDeMierda, null, 4), function (err) {
      if (err) console.error('error guardando archivo de votación actual :(')
    })
  }

  function calcularPorcentajes () {
    var totalDeVotos = 0
    var votActual = votaciones[state.atVotacion]
    // Primero cuento el total
    Object.keys(votActual).forEach(function (ingrediente, index) {
      totalDeVotos += votaciones[state.atVotacion][ingrediente]['cantidad']
    })
    if (totalDeVotos !== 0) {
      // Calculo el porcentaje para cada elemento
      Object.keys(votActual).forEach(function (ingrediente, index) {
        votActual[ingrediente]['porcentaje'] =
        Math.floor(votActual[ingrediente]['cantidad'] * 100 / totalDeVotos)
      })
    }
  }

  function setGanadorImg (puntajes) {
    var ganador = 0
    var scoreGanador = 0
    var g = false
    // Fijate si es el ganador
    console.log(puntajes)
    Object.keys(puntajes).forEach(function (ing, idx) {
      // si hay
      if (puntajes[ing] > scoreGanador) {
        ganador = idx
        scoreGanador = puntajes[ing]
        g = true
      }
    })

    if (!g) ganador = 13
    console.log('el ganador por ahora es: ' + ganador)
    return state.IMG_PATH_ROOT.replace('SHUTA', ganador)
  }
}

/* porcentaje
   cantidadDeEsto * 100 / cantidadTotal
   */
