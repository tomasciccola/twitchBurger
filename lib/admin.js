module.exports = adminManager
require('colors')

function adminManager (state, votaciones) {
  // Esta funcion se corre cada vez que llega un mensaje de admin
  return function (message) {
    if (message === '!RESET') {
      console.log('reseteando votacion'.yellow)
      resetearVotacionActual()
    }

    if (message === '!EMPEZAR') {
      console.log('votacion activada'.yellow)
      state.votando = true
    }

    if (message === '!TERMINAR') {
      console.log('votacion terminada'.yellow)
      state.votando = false
    }

    if (message.indexOf('!VOTAR') !== -1) {
      var n = message.split('!VOTAR')[1]
      if (n > 0 && n < state.N_VOTACIONES) {
        empezarVotacion(parseInt(n - 1))
        console.log(('nueva votación: ' + state.atVotacion).yellow)
      }
    }

    if (message.indexOf('!BLO' !== -1)) {
      var ing = message.split('!BLO')[1].toLowerCase()
      if (votaciones[state.atVotacion][ing] !== undefined) {
        console.log(('bloqueando: ' + ing).yellow)
        votaciones[state.atVotacion][ing]['bloqueado'] =
          !votaciones[state.atVotacion][ing]['bloqueado']
      }
    }
  }

  function resetearVotacionActual () {
    var votActual = votaciones[state.atVotacion]
    Object.keys(votActual).forEach(function (ingrediente) {
      votActual[ingrediente]['cantidad'] = 0
      votActual[ingrediente]['porcentaje'] = 0
    })
  }

  function empezarVotacion (n) {
    state.atVotacion = n
    resetearVotacionActual()
  }
}
