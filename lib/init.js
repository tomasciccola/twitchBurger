const fs = require('fs')
const path = require('path')
require('colors')

module.exports = init

function init (state) {
  if (!fs.existsSync(path.join(process.cwd(), 'data/votaciones.json'))) {
    console.log('generando nuevo objeto de votaciones inicial ya que no existía uno'.yellow)
    return generateVotObj()
  } else {
    console.log('cargando archivo de votaciones ya existente'.yellow)
    return require(path.join(process.cwd(), './data/votaciones.json'))
  }

  function generateVotObj () {
    var arr = []

    for (let j = 0; j < state.N_VOTACIONES; j++) {
      let obj = {}

      for (let i = 0; i < state.INGREDIENTES.length; i++) {
        obj[state.INGREDIENTES[i]] = { 'cantidad': 0, 'porcentaje': 0, 'bloqueado': false }
      }
      arr.push(obj)
    }
    return arr
  }
}
